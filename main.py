import signal
import asyncio
import json
from typing import Dict

import aioamqp
from aioamqp.channel import Channel
from aioamqp.envelope import Envelope
from aioamqp.properties import Properties
from arsenic import stop_session
from ruamel.yaml import YAML

import log
logger = log.setup_logger('main')

from worker import WorkerYandex, WorkerGoogle, GoogleBanException


class Statistic(object):

    workers_class = {
        WorkerYandex.TYPE: WorkerYandex,
        WorkerGoogle.TYPE: WorkerGoogle,
    }

    workers = {
        WorkerYandex.TYPE: {},
        WorkerGoogle.TYPE: {}
    }

    last_worker: int = None

    worker_channel : Channel = None

    data_channel : Channel = None

    config : Dict = None

    work : bool = True

    def __init__(self):
        yaml = YAML()
        with open('config.yml') as file:
            self.config = yaml.load(file)

            asyncio.get_event_loop().add_signal_handler(signal.SIGQUIT, self.sync_stop)

    async def create_channels(self):
        transport, protocol = await aioamqp.from_url(self.config['rabbit']['url'])
        self.worker_channel = await protocol.channel()
        self.data_channel = await protocol.channel()
        await self.worker_channel.queue_declare(queue_name=self.config['rabbit']['queue_name'])
        await self.data_channel.queue_declare(queue_name='data')

    async def start_rabbit(self):
        await self.create_channels()
        await self.worker_channel.basic_consume(self.rabbit_handler, queue_name=self.config['rabbit']['queue_name'])

    def sync_stop(self, *args, **kwargs):
        self.work = False
        asyncio.ensure_future(self.stop())

    async def stop(self):
        logger.warning('Stopping worker...')
        for worker in self.workers[WorkerYandex.TYPE].values():
            await stop_session(worker.session)
        self.workers[WorkerYandex.TYPE].clear()
        for worker in self.workers[WorkerGoogle.TYPE].values():
            await stop_session(worker.session)
        self.workers[WorkerGoogle.TYPE].clear()
        asyncio.get_event_loop().stop()

    async def handle_message(self, message: Dict) -> Dict:
        type = message.get('type', None)
        if type is not None and self.work:
            location = message.get('location', None)
            proxy = message.get('proxy', None)
            meta = self.workers.get(type, None)
            if meta is not None:
                worker = meta.get(location['id'], None)
                if worker is None:
                    worker = self.workers_class[type]()
                    await worker.create(location=location, proxy=proxy)
                    worker.rucaptcha_key = self.config['worker']['rucaptcha_key']
                    self.workers[type][location['id']] = worker
                if message['type'] == 2 and self.last_worker == id(worker):
                    await asyncio.sleep(10)
                try:
                    result = await worker.get_position(keyword=message['keyword'])
                    self.last_worker = id(worker)
                    worker.position = 0
                    result.update({'message': message})
                    return result
                except GoogleBanException:
                    logger.warning('Restarting worker...')
                    await stop_session(worker.session)
                    del self.workers[worker.TYPE][location['id']]
                    return await self.handle_message(message)

    async def rabbit_handler(self, channel: Channel, body: bytes, envelope: Envelope, properties: Properties):
        message = json.loads(body.decode('utf-8'))
        res = await self.handle_message(message)

        await self.data_channel.basic_publish(payload=json.dumps(res), exchange_name='', routing_key='data')
        await self.worker_channel.basic_client_ack(delivery_tag=envelope.delivery_tag)


if __name__ == '__main__':
    statistic = Statistic()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(statistic.start_rabbit())
    loop.run_forever()

