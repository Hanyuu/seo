import logging
from ruamel.yaml import YAML


def setup_logger(name):
    yaml = YAML()
    with open('config.yml') as file:
        config = yaml.load(file)
        logger = logging.getLogger(name)
        if not len(logger.handlers):
            formatter = logging.Formatter(fmt=config['logger']['formatter'])

            handler = logging.StreamHandler()
            handler.setFormatter(formatter)


            logger.setLevel(config['logger']['level'])
            logger.addHandler(handler)
            return logger