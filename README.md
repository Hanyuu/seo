
Protocol description.


type 1 - Yandex


type 2 - Google


{
    "type": 1,
    
    "keyword": {"keyword": "You keyword where","site": "You site where"},
    
    "location": {"id": Unique ID integer, "lat": Location latitude, "lng": Location longitude, "name": "Location name (Only for Yandex)"}
    
}

Example For Yandex:

{
    "type": 1,
    
    "keyword":{"keyword": "Python wikipedia", "site": "wikipedia.org"},
    
    "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": "Москва"}
    
}

Example For Google:

{
    "type": 2,
    
    "keyword":{"keyword": "Python wikipedia", "site": "wikipedia.org"},
    
    "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": "Москва"}
    
}

Response:

{
    "position": 1,
    
    "link": "https://ru.wikipedia.org/wiki/python",
    
    "message":
    
             {
             
                "type": 1, "keyword": {"keyword": "Python wikipedia", "site": "wikipedia.org"},
                
                "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": 'Москва"}
                
             }
             
}

You can use HTTPS proxy.

{
    "type": 2,

    "keyword":{"keyword": "Python wikipedia", "site": "wikipedia.org"},

    "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": "Москва"}

    "proxy": {"base_server": "server_address", "port": "server_port"}

}