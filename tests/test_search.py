import pytest
from main import Statistic


@pytest.mark.asyncio
async def test_google():
    statistic = Statistic()
    res = await statistic.handle_message({"type": 2,
                                     "keyword":
                                         {
                                            "keyword": "Python wikipedia",
                                            "site": "wikipedia.org"
                                          },
                                     "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": "Москва"}
                                     })
    assert 'wikipedia.org' in res['link']
    assert res['position'] == 1
    location = await statistic.workers[2][1].session.get_element_by_xpath('//span[@id="swml-loc"]')
    location_name = await location.get_text()
    assert 'Москва' in location_name


@pytest.mark.asyncio
async def test_yandex():
    statistic = Statistic()
    res = await statistic.handle_message({"type": 1,
                                     "keyword":
                                         {
                                            "keyword": "Python wikipedia",
                                            "site": "wikipedia.org"
                                          },
                                     "location": {"id": 1, "lat": 55.7558, "lng": 37.6173, "name": "Москва"}
                                     })
    assert 'wikipedia.org' in res['link']
    assert res['position'] == 1
    await statistic.workers[1][1].session.get('https://yandex.ru/')
    location = await statistic.workers[1][1].session.get_element_by_xpath('//span[@class="geolink__reg"]')
    location_name = await location.get_text()
    assert 'Москва' in location_name

