import abc
import logging
import os
import re
from functools import partial
from json.decoder import JSONDecodeError
from typing import List, Dict, Awaitable
import aiofiles
import bs4
from aioamqp.channel import Channel
from aiohttp.client_exceptions import ClientOSError
from arsenic import keys, services, Session, start_session
from arsenic.errors import NoSuchElement, StaleElementReference, ArsenicTimeout, NoSuchAlert
from arsenic.session import Element
from python_rucaptcha.ReCaptchaV2 import aioReCaptchaV2

from .firefox import Firefox

import log
logger = log.setup_logger('worker')
class By:
    XPATH = 'xpath'
    CSS_SELECTOR = 'css selector'


class GoogleBanException(Exception):
    pass


class GoogleCaptchaException(Exception):
    pass


class SeleniumSession(Session):

    async def get_element_by(self, by: str, selector: str) -> Element:
        element_id = await self._request(
            url='/element',
            method='POST',
            data={
                'using': by,
                'value': selector
            }
        )
        return self.create_element(element_id)

    async def get_title(self) -> bool:
        return await self._request(
            url='/title',
            method='GET'
        )

    async def get_elements_by(self, by: str, selector: str) -> List[Element]:
        result = await self._request(
            url='/elements',
            method='POST',
            data={
                'using': by,
                'value': selector
            }
        )
        return [self.create_element(element_id) for element_id in result]

    async def get_element_by_xpath(self, selector: str) -> Element:
        return await self.get_element_by(By.XPATH, selector)

    async def wait_element_visible(self, timeout: int, selector: str) -> Awaitable:
        return await self.wait(
            timeout,
            partial(self.is_visible, selector),
            NoSuchElement
        )

    async def wait_for_element_has_text(self, timeout: int, by: str, selector: str, text: str) -> Awaitable:
        return await self.wait(
            timeout,
            partial(self.has_text, by, selector, text),
            NoSuchElement
        )

    async def wait_yandex_has_autocomplite_location(self, timeout: int, location_name: str) -> Awaitable:
        return await self.wait(
            timeout,
            partial(self.yandex_has_autocomplite_location, location_name),
            NoSuchElement
        )

    async def yandex_has_autocomplite_location(self, location_name) -> bool:
        auto_locations = await self.get_elements_by(By.XPATH, '//div[@class="b-autocomplete-item__reg"]')
        for location in auto_locations:
            if location_name in await location.get_text():
                return True
        return False

    async def wait_google_settings_displayed(self, timeout: int) -> Awaitable:
        return await self.wait(
            timeout,
            self.google_settings_displayed,
            NoSuchElement
        )

    async def wait_google_has_no_captha_key(self, timeout: int) -> Awaitable:
        return await self.wait(
            timeout,
            self.google_has_no_captha_key,
            NoSuchElement
        )

    async def wait_google_has_keyword_in_title(self, timeout: int, title: str) -> Awaitable:
        return await self.wait(
            timeout,
            partial(self.has_title, title),
            NoSuchElement
        )

    async def google_settings_displayed(self) -> bool:
        el = await self.get_element_by_xpath('//div[@id="result_slider"]')
        return await el.is_displayed()

    async def google_has_no_captha_key(self) -> bool:
        try:
            input = await self.get_element_by_xpath('//textarea[@id="g-recaptcha-response"]')
            value = await input.get_attribute('value')
            if value is None:
                raise GoogleBanException()
            return len(value) == 0
        except NoSuchElement:
            return True

    async def has_title(self, title_str: str) -> bool:
        return title_str in await self.get_title()

    async def has_url(self, url: str) -> bool:
        return url in await self.get_url()

    async def is_visible(self, selector: str) -> bool:
        element = await self.get_element_by_xpath(selector)
        return await element.is_displayed()

    async def has_text(self, by: str, selector: str, text: str) -> bool:
        element = await self.get_element_by(by, selector)
        return text in await element.get_text()

    async def move_foother(self) -> None:
        await self.execute_script('window.scrollTo(0, document.body.scrollHeight);')

    async def save_screen(self, name: str) -> None:
        async with aiofiles.open(name, 'wb') as file:
            screen = await self.get_screenshot()
            await file.write(screen.read())
            await file.flush()


class Worker(Firefox, metaclass=abc.ABCMeta):
    location_set: bool = False
    settings_set: bool = False
    session: SeleniumSession = None
    session_class: SeleniumSession = SeleniumSession
    timeout = 30
    bs4 = partial(bs4.BeautifulSoup, features='html.parser')
    position = 0
    rucaptcha_key = ''
    words_re = re.compile(r'\w{3,}')
    session_created = False
    data_channel: Channel = None
    task_id = None

    async def create_session(self, timeout: int = 30):
        if not self.session_created:
            self.timeout = timeout
            if self.session is None:
                service = services.Geckodriver()
                service.log_file = os.devnull
                self.session = await start_session(service, self)
            self.session_created = True

    @abc.abstractmethod
    async def get_position(self, keyword: Dict, page: int = 0) -> Dict:
        pass

    @abc.abstractmethod
    async def set_location(self) -> None:
        pass

    @abc.abstractmethod
    async def check_captcha(self) -> bool:
        pass


class WorkerYandex(Worker):
    TYPE = 1

    async def create_session(self, timeout: int = 30) -> None:
        await super().create_session(timeout)
        if not self.settings_set:
            await self.set_settings()
        if not self.location_set:
            await self.set_location()

    async def get_position(self, keyword: Dict, page: int = 0) -> Dict:
        await self.create_session()
        await self.session.get('https://yandex.ru/search/?text={keyword}&p={page}'.format(keyword=keyword['keyword'], page=page))
        source = await self.session.get_page_source()
        bs = self.bs4(source)
        results = bs.find_all('h2', {'class': 'organic__title-wrapper'})
        for result in results:
            a = result.find('a', {'target': '_blank'})
            if a is not None:
                href = a['href'].lower()
                if not 'yandex' in href:
                    self.position += 1
                    if keyword['site'] in href:
                        return {'position': self.position, 'link': href}
        if page < 4:
            return await self.get_position(keyword, page + 1)
        else:
            return {'position': -1, 'link': None}

    async def set_settings(self) -> None:
        await self.session.get('https://yandex.ru/search/customize')
        set_50 = await self.session.get_element_by_xpath('//dl[@class="field form__docs-count"]//label[4]')
        await set_50.click()
        save = await self.session.get_element_by_xpath('//div[@class="form__submit"]//button')
        await save.click()
        self.settings_set = True

    async def set_location(self) -> None:
        if self.location is not None:
            await self.session.get('https://yandex.ru/')
            location = await self.session.get_element_by_xpath('//span[@class="geolink__reg"]')
            await location.click()
            input = await self.session.get_element_by_xpath('//input[@id="city__front-input"]')
            current_location = await input.get_attribute('value')
            if self.location['name'] != current_location:
                await input.clear()
                await input.send_keys(self.location['name'])
                await self.session.wait_yandex_has_autocomplite_location(self.timeout, self.location['name'])
                await input.send_keys(keys.ENTER)
                await self.session.wait_for_element(self.timeout, 'input[id="text"]')
            self.location_set = True

    async def check_captcha(self):
        pass


class WorkerGoogle(Worker):
    site_key = '6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1b'
    max_page = 12
    TYPE = 2

    async def create_session(self, timeout: int = 30) -> None:
        if not self.session_created:
            await super().create_session(timeout)
            await self.session.get('https://www.google.com/')
            self.session_created = True

    async def get_position(self, keyword: Dict, page: int = 1) -> Dict:
        await self.create_session()
        await self.check_captcha()
        if not await self.check_ban():
            try:
                if page > 1:
                    set_page = await self.session.get_element_by_xpath('//a[@aria-label="Page {}"]'.format(page))
                    await self.session.move_foother()
                    await set_page.click()
                    await self.session.wait_for_element(5, '#res')
                else:
                    input = await self.session.get_element_by_xpath('//input[@id="lst-ib"]')
                    await input.clear()
                    await input.send_keys(keyword['keyword'])
                    await input.send_keys(keys.ENTER)
                    await self.session.wait_google_has_keyword_in_title(5, keyword['keyword'])
                    await self.session.wait_for_element(5, '#res')
            except (ArsenicTimeout, NoSuchElement):
                await self.check_captcha()

            if not self.location_set:
                await self.set_location()
                btn_g = await self.session.get_element_by_xpath('//button[@name="btnG"]')
                await btn_g.click()

            source = await self.session.get_page_source()
            bs = self.bs4(source)
            results = bs.find_all('h3', {'class': 'r'})
            for result in results:
                href = result.find('a')
                if href is not None:
                    href = href['href'].lower()
                    if not 'google' in href:
                        self.position += 1
                        if keyword['site'] in href:
                            return {'position': self.position, 'link': href}
            page = page + 1
            if page < self.max_page:
                return await self.get_position(keyword=keyword, page=page)
            else:
                return {'position': -1, 'link': None}
        else:
            logger.error(f'Proxy {self.proxy} is banned on Google')
            raise GoogleBanException()

    async def set_location(self) -> None:
        await self.session.move_foother()
        await self.session.wait_for_element(self.timeout, 'a[id="swml-upd"]')
        set_location = await self.session.get_element_by_xpath('//a[@id="swml-upd"]')
        await self.session.wait_element_visible(self.timeout, '//a[@id="swml-upd"]')
        await set_location.click()
        await self.session.wait_for_element(self.timeout, '.known_loc')
        self.location_set = True

    async def check_captcha(self) -> bool:
        if await self.session.has_url('ipv4.google.com'):
            logger.error('Google has captcha!')
            try:
                await self.session.wait_for_element(5, 'textarea[id="g-recaptcha-response"]')
                await self.session.execute_script('document.getElementById("g-recaptcha-response").style.display="block";')
            except ArsenicTimeout:
                return False
            try:
                url = await self.session.get_url()
                ru_captcha = aioReCaptchaV2(rucaptcha_key=self.rucaptcha_key)
                answer_usual_re2 = await ru_captcha.captcha_handler(site_key=self.site_key, page_url=url)
                if answer_usual_re2['errorBody'] is None:
                    try:
                       g_recaptcha = await self.session.get_element_by_xpath('//textarea[@id="g-recaptcha-response"]')
                       await g_recaptcha.send_keys(answer_usual_re2['captchaSolve'])
                       send = await self.session.get_element_by_xpath('//input[@type="submit"]')
                       await send.click()
                       await self.session.wait_google_has_no_captha_key(self.timeout)
                    except (StaleElementReference, NoSuchElement, ArsenicTimeout):
                        return False
                else:
                    logger.error(f'Captcha solving has errer {answer_usual_re2["errorBody"]}')
                    return await self.check_captcha()
            except (JSONDecodeError, ClientOSError) as e:
                logger.error(f'Error captcha handling {e}')
                return await self.check_captcha()
        else:
            return False

    async def has_google(self) -> bool:
        try:
            await self.session.get_element_by_xpath('//img[@alt="Google"]')
            return True
        except NoSuchElement:
            return False

    async def check_ban(self) -> bool:
        try:
            await self.session.wait_for_element(5, 'input[id="lst-ib"]')
            return False
        except ArsenicTimeout:
            try:
                await self.session.wait_for_element(5, 'textarea[id="g-recaptcha-response"]')
                return False
            except ArsenicTimeout:
                return True